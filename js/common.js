var utils = {};

/**
 * 加法运算，避免数据相加小数点后产生多位数和计算精度损失。
 * 
 * @param num1加数1 | num2加数2
 */
utils.numAdd = function (num1, num2, retain) {
	retain = retain || retain == 0 ? retain : 2;
    var baseNum, baseNum1, baseNum2; 
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
    var result = (num1 * baseNum + num2 * baseNum) / baseNum;
    return result.toFixed(retain);
};

/**
 * 减法运算，避免数据相减小数点后产生多位数和计算精度损失。
 * @param num1被减数  |  num2减数
 */
utils.numSub = function (num1, num2, retain) {
	retain = retain || retain == 0 ? retain : 2;
    var baseNum, baseNum1, baseNum2;
    var precision;// 精度
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
    // precision = (baseNum1 >= baseNum2) ? baseNum1 : baseNum2;
    var result = (num1 * baseNum - num2 * baseNum) / baseNum;
    return result.toFixed(retain);
};
/**
 * 乘法运算，避免数据相乘小数点后产生多位数和计算精度损失。
 * @param num1被乘数 | num2乘数
 */
utils.numMulti = function (num1, num2, retain) {
	retain = retain || retain == 0 ? retain : 2;
    var baseNum = 0;
    try {
        baseNum += num1.toString().split(".")[1].length;
    } catch (e) {
    }
    try {
        baseNum += num2.toString().split(".")[1].length;
    } catch (e) {
    }
    var result = Number(num1.toString().replace(".", "")) * Number(num2.toString().replace(".", "")) / Math.pow(10, baseNum);
    return result.toFixed(retain);
};
/**
 * 除法运算，避免数据相除小数点后产生多位数和计算精度损失。
 * @param num1被除数 | num2除数
 */
utils.numDiv = function (num1, num2, retain) {
	retain = retain || retain == 0 ? retain : 2;
    var baseNum1 = 0, baseNum2 = 0;
    var baseNum3, baseNum4;
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    baseNum3 = Number(num1.toString().replace(".", ""));
    baseNum4 = Number(num2.toString().replace(".", ""));
    var result = (baseNum3 / baseNum4) * Math.pow(10, baseNum2 - baseNum1);
    return result.toFixed(retain);
};
/**
 * 把价格单位为分的转换为角单位。
 * @param num 价格
 * @param retain 保留小数位数
 */
utils.pointToCorner = function (num, retain) {
	retain = retain || retain == 0 ? retain : 2;
    var value = utils.numDiv(+num, 100, retain),
        result = utils.dealPricePoint(value);
    return result;
}
/**
 * 计算预估可赚金额。
 * @param rate 佣金比
 * @param num 金额
 * @param rateRetain 佣金比例保留小数位数
 * @param rateUnit 佣金比例单位(默认：千分)
 * @param earnRetain 预估可赚金额保留小数位数
 */
utils.canEarn = function (rate, num, rateRetain, rateUnit, earnRetain) {
	rateRetain = rateRetain || rateRetain == 0 ? rateRetain : 4;
	rateUnit = rateUnit || rateUnit == 0 ? rateUnit : 1000;
	earnRetain = earnRetain || earnRetain == 0 ? earnRetain : 2;
    var temp = utils.numDiv(+rate, rateUnit, rateRetain),
        temp2 = utils.numMulti(+num, +temp, earnRetain),
        result = utils.dealPricePoint(temp2);
    return result;
}
/**
 * 处理价格小数后位数展示。
 * @param price 价格
 */
utils.dealPricePoint = function (price) {
    if(!price) return;
    var arr = price.split('.'),
        obj = [];
    if(arr[1]) {
        let arr1 = arr[1].split('').reverse();
        for(let i in arr1) {
            if(arr1[i] != '0') obj.push(arr1[i]);
        }
    }
    if(obj.length > 0) {
        obj.push('.');
        obj.reverse();
    }
    obj = obj.join('');
    return arr[0] + obj;
}

utils.getUrlParameter = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}

utils.formatNumber = function(n) {
    var str = n.toString()
    return str[1] ? str : '0' + str;
}

utils.formatTime = function(date, form, hasHour = false) {
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()

    var hour = date.getHours()
    var minute = date.getMinutes()
    var second = date.getSeconds()
    var t1 = '', t2 = '';
    t1 = [year, month, day].map(utils.formatNumber).join(form)
    if(hasHour) t2 = [hour, minute, second].map(utils.formatNumber).join(':')

    return t1 + ' ' + t2;
}